<?php

use Reborn\Cores\Application;

/**
 * Reborn CMS <==(-^_^-)==> PHP Content Management System
 *
 * @package Reborn CMS
 * @author Reborn CMS Development Team
 */



// Define Directory Sperator
define('DS', DIRECTORY_SEPARATOR);

// Change the current dir
chdir(__DIR__);

// Define Base Dir Path
define('BASE', __DIR__ . DS);

// load Reborn CMS start file
require_once __DIR__.'/heart/reborn/src/start.php';

// Set Time and Memory for Application Start
define('REBORN_START_TIME', microtime(true));
define('REBORN_START_MEMORY', memory_get_usage());

// Create Object for Application
$app = new Application();

// Start the Application
$app->start();

//dump(DB::table('settings')->where('slug', '=', 'site_title')->get(array('name')));

//dump(DB::getSchemaBuilder());

/*    Schema::table('tester', function($table)
    {
	    $table->create();
	    $table->increments('id');
	    $table->string('username');
	    $table->string('email');
	    $table->string('phone')->nullable();
	    $table->text('about');
	    $table->timestamps();
    });*/

/*$str = "max:15";

if(($pos = strpos($str, ":")) !== false)
{
	dump(substr($str, $pos+1));
}*/
/*$input = array(
		'age' => '16',
		'age2' => '23',
		'address' => '',
		'email' => 'nyan@gmail.travel.uk',
		'url' => 'http://myanmarlink.com',
		'ip' => '192.168.0.1',
		'level' => '31',
		'space' => 'Whoareyou123456',
		'password' => 'Whoareyou123456',
		'ph' => '01-123456'
	);
$rule = array(
		'age' => 'max:19|min:17',
		'age2'=> 'min:22',
		'address' => 'required|maxLength:50|minLength:16',
		'email' => 'email',
		'url' => 'url',
		'ip' => 'ip',
		'level' => 'between:11,30',
		'ph' => 'maximum:12344',
		'space' => 'equal:Whoareyou12345689'
		'ph' => 'pattern:#^(\d{2}-\d{6}+)$#'
	);

$v = new Reborn\Cores\Validation($input, $rule);
//$v = Reborn\Cores\Validation::create($input, $rule);
$msg = 'Hey {key} You need to same to with {maximum}';
$v->addRule('maximum', $msg, function($value, $no){
	return ($value < $no);
});

if($v->fail())
{
	echo 'I am Validation Fail by fail method';
}
else
{
	echo 'I am Valid by using fail method else';
}

dump($v->getErrors());*/

?>
