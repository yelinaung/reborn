<?php

namespace Sample\Controller;

class Controller_Sample extends \Reborn\Cores\Controller
{
	protected $useOrm = true;
	public function before()
	{
		parent::before();
		return '-----===== I am Before for sample module =====-----<br>';
	}

	public function index()
	{
		//\Reborn\Cores\Module::install('pages');
		//\Reborn\Cores\Module::uninstall('pages');
		\Reborn\Cores\Module::upgrade('pages');
		//$r = new \Pages\Controller_Pages();
		//echo $r->index();
		//\Reborn\Cores\Module::load('blog');
		//$b = new \Blog\Controller_Blog();
		//echo $b->index();

		return 'Sample Module index use ORM and insert ID is <br>'; //.$p2->getTitle();
	}

	public function view($file)
	{
		$path = realpath(__DIR__.'/../views/').DS.$file.EXT;

		if(file_exists($path))
		{
			$content = \Reborn\Cores\File::getContent($path);
		}
		else
		{
			$content = 'No file exits';
		}
		echo $content;
		//echo 'I am Blog View Method. I am '.$file.' Age is '.$age.'. Job is '.$job;
	}

	public function after($response)
	{

		$response .= '<br>-----===== I am After for SAMPLE module =====-----<br>';

		return parent::after($response);
	}
}
