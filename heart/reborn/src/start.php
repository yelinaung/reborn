<?php

// Define Content Path
if(! defined('CONTENT'))
{
	define('CONTENT', realpath(BASE.'content/').DS);
}

// Define Upload Path
if(! defined('UPLOAD'))
{
	define('UPLOAD', realpath(CONTENT.'uploads/').DS);
}

// Define Modules Path
if(! defined('MODULES'))
{
	define('MODULES', realpath(CONTENT.'modules/').DS);
}

// Define Themes Path
if(! defined('THEMES'))
{
	define('THEMES', realpath(CONTENT.'themes/').DS);
}

// Define System Path
if(! defined('SYSTEM'))
{
	define('SYSTEM', realpath(BASE.'/heart/').DS);
}

// Define Storages Path
if(! defined('STORAGES'))
{
	define('STORAGES', realpath(SYSTEM.'reborn/src/Storages/').DS);
}

// Define System's Cores Path
if(! defined('CORES'))
{
	define('CORES', realpath(SYSTEM.'reborn/src/Reborn/').DS);
}

// Define Core Modules Path
if(! defined('CORE_MODULES'))
{
	define('CORE_MODULES', realpath(SYSTEM.'/reborn/src/RebornModules/').DS);
}

// Define Admin Themes Path
if(! defined('ADMIN_THEME'))
{
	define('ADMIN_THEME', realpath(SYSTEM.'/themes/').DS);
}

if(! defined('EXT'))
{
	define('EXT', '.php');
}

// Require Autoload File
require_once SYSTEM.'vendor/autoload.php';

// Class Alias for Reborn Cores Alias Class
class_alias('Reborn\Cores\Alias', 'Alias');
Alias::coreClassAlias();
