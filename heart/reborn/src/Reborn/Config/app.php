<?php

return array(

	/**
	 * Base URL for Reborn CMS
	 * URL must contain (/)
	 *
	 */
	'base_url' => '',

	/**
	 * Default Language for Reborn CMS
	 */
	'lang' => 'en',

	/**
	 * Supported Language for Reborn CMS
	 * Now only support english only
	 *
	 */
	'support_langs' => array(
			'en' => 'en',
		),

	/**
	 * Default Timezone for Reborn CMS
	 */
	'timezone' => 'UTC',

	/**
	 * Default Locale for Reborn CMS
	 */
	'locale' => 'en',

	/**
	 * Fallback Locale for Reborn CMS
	 * Fallback locale is use on default locale is does not work.
	 * Don't change this locale!
	 *
	 */
	'fallback_locale' => 'en',

	/**
	 * Character Encoding for Reborn CMS
	 */
	'encoding' => 'UTF-8',

	/**
	 * Config Values for Event Class
	 */
	'event' => array(

			// If event callback is not callable, throw exception
			// Use for development only
			'callback_throw' => false,
		),

	/**
	 * Config Values for Logging process
	 */
	'log' => array(

			// Path for saving log file
			'path' => STORAGES.'Logs'.DS,

			// log file name use at saving process (eg: rebornlog-20121216.log)
			'file_name' => 'rebornlog-'.Date('Ymd'),

			// Log file extension (default is .log). Don't forget "dot"!
			'ext' => '.log',
		),

);
