<?php

// This is default route for Reborn CMS
// Don't delete this.
Route::add('/', array(
		'module' => 'pages',
		'controller' => 'pages',
		'action' => 'index',
	));

/*Route::add('pages', array(
		'module' => 'page',
		'controller' => 'page',
		'action' => 'index',
	));*/

Route::add('pages/{:name}', function($name){
	echo 'Hello '.$name.', How are you?';
});

