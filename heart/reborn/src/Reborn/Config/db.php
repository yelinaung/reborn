<?php

return array(

	/**
	 * Active (Default) Connection Config name
	 */
	'active' => 'default',

	/**
	 * Default DB Connection Config
	 *
	 */
	'default' => array(
			// Database Driver name (support driver : mysql, pgsql, sqlite, sqlsrv)
			// But Reborn is testing only mysql
			'driver'	=> 'mysql',
			'host'		=> 'localhost',
			'database'	=> 'rrbt',
			'username'	=> 'root',
			'password'	=> '',
			'port'		=> 3306,
			'charset'	=> 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'	=> ''
		),

);
