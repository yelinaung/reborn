<?php

return array(
		array(
				'name' => 'reborn.app.starting',
				'callback' => 'reborn_app_starting'
			),
		array(
				'name' => 'reborn.app.startroute',
				'callback' => 'reborn_app_startroute'
			),
		array(
				'name' => 'reborn.app.endroute',
				'callback' => 'reborn_app_endroute'
			),
		array(
				'name' => 'reborn.app.ending',
				'callback' => 'reborn_app_ending'
			)
	);
