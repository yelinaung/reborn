<?php

namespace Reborn\Cores;

/**
 * Array Method Collection Class
 * (Helper functions for array)
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class Methods_ArrayMethods
{

    /**
     * Array flatten method
     *
     * @param array|object $array
     * @param array $return Return result. Default is empty array
     * @return array
     */
    public static function flatten($array, $return = array())
    {
        foreach ($array as $key => $value)
        {
            if (is_array($value) || is_object($value))
            {
                $return = self::flatten($value, $return);
            }
            else
            {
                $return[] = $value;
            }
        }

        return $return;
    }

    /**
     * Clean the given array
     *
     * @param array $array
     * @return void
     **/
    public static function clean($array)
    {
        return array_filter($array, function($item) {
                return !empty($item);
            });
    }

    /**
     * Trim the given array
     *
     * @param array $array
     * @return array
     */
    public static function trim($array)
    {
        return array_map(function($item) {
                return trim($item);
            }, $array);
    }

    /**
     * Convert given array to object.
     *
     * @param array $array
     * @return object
     */
    public static function toObject($array)
    {
        $result = new \stdClass();

        foreach ($array as $key => $value)
        {
            if (is_array($value))
            {
                $result->{$key} = self::toObject($value);
            }
            else
            {
                $result->{$key} = $value;
            }
        }

        return $result;
    }

    /**
     * Return the odd key of given array
     *
     * @param array $array
     * @return array
     */
    public static function odd($array)
    {
        return array_filter($array, function($item){
                 return($item & 1);
            });
    }

    /**
     * Return the even key of given array
     *
     * @param array $array
     * @return array
     */
    public static function even($array)
    {
        return array_filter($array, function($item){
                 return(!($item & 1));
            });
    }

} // END ArrayMethods
