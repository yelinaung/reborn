<?php

namespace Reborn\Cores;

/**
 * Translate Class for Reborn CMS.
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class Translate
{
	/**
	 * Variable for locale
	 *
	 * @var string
	 **/
	public static $locale = null;

	/**
	 * Variable for fallback locale
	 *
	 * @var string
	 **/
	public static $fallback_locale = 'en';

	/**
	 * Variable for File path (Module Path, Theme Path)
	 *
	 * @var array
	 **/
	protected static $paths = array();

	public function __construct($locale, $fallback_locale = 'en', )
	{
		static::setPath();
	}

	/**
	 * Set the Language File Path.
	 * Default path are [ CORE_MODULES, MODULES, THEMES, ADMIN_THEME ]
	 *
	 * @param array $paths Path array for language file.
	 * @return void
	 */
	public static function setPath($path = array())
	{
		if( empty(static::$paths) )
		{
			static::$paths = array_merge( static::$paths, array(CORE_MODULES, MODULES, THEMES, ADMIN_THEME) );
		}

		if(! empty($path) )
		{
			static::$path = array_push(static::$paths, $path);
		}
	}

	public static function instance($locale, $fallback_locale = 'en')
	{
		static::setPath();
	}

	public static function load($resource, $locale, $type = 'file')
	{
		$loaderClass = ucfirst($type).'Loader';

		if(class_exists($loaderClass))
		{
			$class = new $loaderClass(static::$paths, static::$locale,);
		}
		#TODO
	}
} // END class Translate
