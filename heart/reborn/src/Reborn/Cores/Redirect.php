<?php

namespace Reborn\Cores;

/**
 * Redirect class for Reborn CMS
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class Redirect extends \Symfony\Component\HttpFoundation\RedirectResponse
{
	public static function to($url, $status = 302, $headers = array())
	{
		if(0 == strpos($url, "://"))
		{
			$url = Uri::create($url);
		}

		return new static($url, $status);
	}

} // END class Redirect
