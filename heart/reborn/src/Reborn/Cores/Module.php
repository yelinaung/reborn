<?php

namespace Reborn\Cores;

use Composer\Autoload\ClassLoader as Loader;
use Reborn\Cores\Exception\ModuleException as ModuleException;
use Reborn\Cores\Module\InitializeInterface as InitializeInterface;

/**
 * Module Class for Reborn CMS
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class Module
{
	/**
	 * Variable for Reborn CMS's modules
	 *
	 * @var array
	 **/
	protected static $modules = array();

	/**
	 * Variable for loaded modules
	 *
	 * @var array
	 **/
	protected static $loadedModules = array();

	/**
	 * Variable for Setting table
	 *
	 * @var string
	 **/
	protected static $_table = 'modules';

	/**
	 * Variables for module exception message
	 *
	 * @var string
	 */
	protected static $notFound = 'Module name { %s } doesn\'t exits in module path.';
	protected static $alreadyInstalled = 'Module { %s } is already installed.';
	protected static $noModFile = 'Module.php not found in given { %s }.';
	protected static $modFileRule = 'Module.php of { %s } is not match with Reborn Module rule';

	/**
	 * Initialize method for module class
	 */
	public static function initialize()
	{
		// Find all module from Module Folders
		$modulesFromFolder = static::findAll();

		// Find all module list from DB
		$modulesFromDB = static::findFromDB();

		// Setup the all modules
		static::moduleSetup($modulesFromFolder, $modulesFromDB);
	}

	/**
	 * Get module data by given module name
	 *
	 * @param string $module Name of modules
	 * @return array|null
	 **/
	public static function getData($module)
	{
		$module = ucfirst($module);
		if(isset(static::$modules[$module]))
		{
			return static::$modules[$module];
		}

		return null;
	}

	/**
	 * Check the given module is exists or not
	 *
	 * @param string $module Name of module
	 * @return bool
	 **/
	public static function exits($module)
	{
		$module = ucfirst($module);

		return isset(static::$modules[$module]) ? true : false;
	}

	/**
	 * Load the given module
	 *
	 * @param string $module
	 * @return void
	 **/
	public static function load($module)
	{
		$module = ucfirst($module);

		if(!class_exists('Composer\Autoload\ClassLoader'))
		{
			throw new RbException("We need \"Composer\Autoload\ClassLoader\"");
		}

		if(isset(static::$loadedModules[$module]))
		{
			return true;
		}

		if(static::isCore($module))
		{
			$path =  CORE_MODULES;
		}
		else
		{
			$path =  MODULES;
		}

		if(isset(static::$modules[$module]) and static::$modules[$module]['enabled'])
		{
			$loader = new Loader();
			$loader->add($module, $path);
			$loader->register();
			static::$loadedModules[$module] = true;
		}
	}

	/**
	 * Install the given module to DB table
	 *
	 * @param string $module slug of module(folder name)
	 * @return void
	 */
	public static function install($module, $setEnable = false)
	{
		$module = ucfirst($module);
		// Check the given module is truely
		// If module is true, return the data array from module.php
		$data = static::checkModule($module);

		if(false === static::$modules[$module]['installed'])
		{
			$desc = isset($data['info']['description']) ? $data['info']['description'] : '';
			// Install the Module Data into DB table
			$id = DB::table(static::$_table)
					->insertGetId(array(
							'slug' => $module,
							'name' => $data['info']['name'],
							'description' => $desc,
							'enabled' => ($setEnable) ? 1 : 0,
							'version' => $data['info']['version']
						)
					);
			if($id)
			{
				$iniFile = static::$modules[$module]['path'].'Installer'.DS.'Initialize'.EXT;
				// If given module has Initialize.php File, call the install method
				if(file_exists($iniFile))
				{
					$class = ucfirst($module).'\Installer\Initialize';
					if(! class_exists($class))
					{
						require $iniFile;
						$class = new $class();
					}

					if($class instanceof InitializeInterface)
					{
						$class->install();
					}
				}
			}
		}
		else
		{
			throw new ModuleException(sprintf(static::$alreadyInstalled, $module));
		}

	}

	/**
	 * UnInstall the given module to DB table
	 *
	 * @param string $module slug of module(folder name)
	 * @return void
	 */
	public static function uninstall($module)
	{
		$module = ucfirst($module);
		// Check the given module is truely
		// If module is true, return the data array from Module.php
		$data = static::checkModule($module);

		// If module is core, Don't allow to uninstall
		if(!static::isCore($module))
		{
			return false;
		}

		if(false === static::$modules[$module]['installed'])
		{
			return true;
		}

		DB::table(static::$_table)->where('slug', '=', $module)->delete();

		$iniFile = static::$modules[$module]['path'].'Installer'.DS.'Initialize'.EXT;

		// If given module has Initialize.php File, call the uninstall method
		if(file_exists($iniFile))
		{
			$class = ucfirst($module).'\Installer\Initialize';
			if(! class_exists($class))
			{
				require $iniFile;
				$class = new $class();
			}

			if($class instanceof InitializeInterface)
			{
				$class->uninstall();
			}
		}
	}

	/**
	 * Upgrade the given module.
	 *
	 * @param string $module Name of Module
	 * @return void
	 **/
	public static function upgrade($module)
	{
		$module = ucfirst($module);

		$data = static::checkModule($module);

		if(!static::$modules[$module]['upgradeRequire'])
		{
			return false;
		}

		$iniFile = static::$modules[$module]['path'].'Installer'.DS.'Initialize'.EXT;

		// If given module has Initialize.php File, call the uninstall method
		if(file_exists($iniFile))
		{
			$class = ucfirst($module).'\Installer\Initialize';
			if(! class_exists($class))
			{
				require $iniFile;
				$class = new $class();
			}

			if($class instanceof InitializeInterface)
			{
				$class->upgrade(static::$modules[$module]['dbVersion']);
			}
		}

		// Upgrade the DB's Module Version
	}

	/**
	 * Check the given module is core module or not.
	 *
	 * @param string $module Module Name(slug)
	 * @return boolean
	 */
	public static function isCore($module)
	{
		if(is_dir(CORE_MODULES.$module.DS))
		{
			return true;
		}

		return false;
	}

	/**
	 * Set the given module is enable.
	 *
	 * @param string $module
	 * @return void
	 **/
	public static function enable($module)
	{
		$module = ucfirst($module);
		if(isset(static::$modules[$module]) and !static::$moddules[$module]['enabled'])
		{
			DB::table(static::$_table)->where('slug', '=', $module)
									->update(array('enabled' => 1));

			return true;
		}

		return false;
	}

	/**
	 * Set the given module is disable.
	 *
	 * @param string $module
	 * @return void
	 **/
	public static function disable($module)
	{
		$module = ucfirst($module);
		if(isset(static::$modules[$module]) and static::$moddules[$module]['enabled'])
		{
			DB::table(static::$_table)->where('slug', '=', $module)
									->update(array('enabled' => 0));

			return true;
		}

		return false;
	}

	/**
	 * Check given module is enabled or not
	 *
	 * @param string $module
	 * @param boolean $throw If you set true, throw the Exception message
	 * @return boolean
	 **/
	public static function isEnabled($module, $throw = false)
	{
		$module = ucfirst($module);
		if(! isset(static::$modules[$module]))
		{
			if($throw)
			{
				throw new ModuleException(sprintf(static::$notFound, $module));
			}
			return false;
		}

		return static::$modules[$module]['enabled'];
	}

	/**
	 * Check given module is disabled or not
	 *
	 * @param string $module
	 * @param boolean $throw If you set true, throw the Exception message
	 * @return bool
	 **/
	public static function isDisabled($module, $throw = false)
	{
		return ! static::isEnabled($module, $throw);
	}

	/**
	 * Check the given module is truely or not.
	 * First - check the module is really exits
	 * Second - check the module's module.php file
	 * Third - check the module.php format is correct?
	 *
	 * @param string $module
	 * @return void
	 **/
	protected static function checkModule($module)
	{
		if(! static::find($module))
		{
			throw new ModuleException(sprintf(static::$notFound, $module));
		}

		if($data = static::checkModFile($module))
		{
			return $data;
		}
	}

	/**
	 * Check the given module's (module path) module.php file.
	 *
	 * @param string $path Path of module
	 * @return array
	 */
	protected static function checkModFile($module)
	{
		$path = static::$modules[$module]['path'];
		if(! file_exists($path.'Installer'.DS.'Module'.EXT))
		{
			throw new ModuleException(sprintf(static::$noModFile, $module));
		}

		$modData = require $path.'Installer'.DS.'Module'.EXT;

		if(! isset($modData['info']['version']) || ! isset($modData['info']['name']))
		{
			throw new ModuleException(sprintf(static::$modFileRule, $module));
		}

		return $modData;
	}

	/**
	 * Setup the given modules. Module list from Forlder and DB.
	 * Reborn make module is enable or disable at this stage
	 *
	 *
	 * @param array $modulesFromFolder Module list array from module folder
	 * @param array $modulesFromDB Modulelist array from module DB table
	 * @return void
	 **/
	protected static function moduleSetup($modulesFromFolder, $modulesFromDB)
	{
		foreach($modulesFromFolder as $key => $mod)
		{
			// Check folder have on db? If true, this module is installed.
			if(isset($modulesFromDB[$key]))
			{
				$modulesFromFolder[$key]['installed'] = true;
				if($modulesFromDB[$key]['enabled'])
				{
					$modulesFromFolder[$key]['enabled'] = true;
				}
				else
				{
					$modulesFromFolder[$key]['enabled'] = false;
				}

				if($modulesFromDB[$key]['version'] == $modulesFromFolder[$key]['info']['version'])
				{
					$modulesFromFolder[$key]['upgradeRequire'] = false;
				}
				else
				{
					$modulesFromFolder[$key]['upgradeRequire'] = true;
				}
				$modulesFromFolder[$key]['dbVersion'] = $modulesFromDB[$key]['version'];
			}
			else
			{
				$modulesFromFolder[$key]['installed'] = false;
				$modulesFromFolder[$key]['enabled'] = false;
				$modulesFromFolder[$key]['upgradeRequire'] = false;
			}
		}
		static::$modules = $modulesFromFolder;
	}

	/**
	 * Find the module by given name
	 *
	 * @param string $module Name of module
	 * @return void
	 **/
	protected static function find($module)
	{
		if(isset(static::$modules[$module]))
		{
			return static::$modules[$module];
		}

		$paths = array(CORE_MODULES, MODULES);

		foreach($paths as $path)
		{
			if(is_dir($path.$module))
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * Find All modules from Module Folders
	 * Core Modules and Addon Modules
	 *
	 * @return array
	 **/
	protected static function findAll()
	{
		$modPaths = array(CORE_MODULES, MODULES);

		$allFolder = array();

		foreach($modPaths as $path)
		{
			$iterator = new \DirectoryIterator($path);

			foreach($iterator as $dir)
			{
				if(!$dir->isDot() and $dir->isDir())
				{
					$mod_name = $dir->getFileName();
					$mod_path = $dir->getPath().DS.$mod_name.DS;
					if(file_exists($mod_path.'Installer'.DS.'Module'.EXT))
					{
						// Check module name is already exits
						if(isset($allFolder[$mod_name]))
						{
							throw new RbException("Module slug {$mod_name} is already exits.");
						}

						$modFileData =  require $mod_path.'Installer'.DS.'Module'.EXT;

						$modData = static::parseModData($modFileData);

						$allFolder[$mod_name] = $modData;
						$allFolder[$mod_name]['path'] = $mod_path;
						if(preg_match('/RebornModules/', $mod_path))
						{
							$allFolder[$mod_name]['core'] = true;
						}
						else
						{
							$allFolder[$mod_name]['core'] = false;
						}
					}
				}
			}
		}

		return $allFolder;
	}

	/**
	 * Parse the Module.php File data array to App's mod data array
	 *
	 * @param arrya $data Module.php file's data array
	 * @return array
	 */
	protected static function parseModData($data)
	{
		$result = array();
		$result['name'] = $data['info']['name'];
		unset($data['info']['name']);
		if(isset($data['info']['description']))
		{
			$result['description'] = $data['info']['description'];
			unset($data['info']['description']);
		}
		else
		{
			$result['description'] = '';
		}

		$result['info'] = $data['info'];
		$result['roles'] = isset($data['roles']) ? $data['roles'] : array();
		$result['require'] = isset($data['require']) ? $data['require'] : array();

		return $result;
	}

	/**
	 * Find All modules from DB table
	 *
	 * @return array
	 **/
	protected static function findFromDB()
	{
		$modFromDB = array();

		$mods = DB::table(static::$_table)->get();

		foreach($mods as $mod)
		{
			$name = ucfirst($mod->slug);
			$modFromDB[$name]['name'] = $mod->name;
			$modFromDB[$name]['description'] = $mod->description;
			$modFromDB[$name]['enabled'] = ($mod->enabled == '0') ? false : true;
			$modFromDB[$name]['version'] = $mod->version;
		}
		$name = 'Pages';
		$modFromDB[$name]['name'] = $name;
		$modFromDB[$name]['description'] = $name.' is description';
		$modFromDB[$name]['enabled'] = true;
		$modFromDB[$name]['version'] = '0.8';
		return $modFromDB;
	}

} // END class Module
