<?php

namespace Reborn\Cores;

/**
 * Controller Class for Reborn CMS
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
abstract class Controller extends ControllerContainer
{
	/**
	 * Constructor Method
	 *
	 * @return void
	 **/
	public function __construct()
	{
	}

	/**
	 * Before Method for controller.
	 * This method will be call before request action.
	 */
	public function before()
	{
		parent::before();
	}

	/**
	 * After Method for controller.
	 * This method will be call after request action.
	 */
	public function after($response)
	{
		return parent::after($response);
	}

	/**
	 * Render the given view file.
	 *
	 * @param string $file View file name.
	 * @param mixed $data Data for given view file.
	 * @param boolean $escape Escape the given view data.
	 * @return void
	 **/
	protected function render($file, $data = array(), $escape = false)
	{
		//return $this->view->render($file, $data, $escape);
	}

} // END class Controller
