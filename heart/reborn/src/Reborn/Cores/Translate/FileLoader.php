<?php

namespace Reborn\Cores\Translate;

/**
 * Translate File Loader
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class FileLoader implements LoaderInterface
{
	/**
	 * Variable for default locale
	 *
	 * @var string
	 **/
	public static $locale;

	/**
	 * undocumented class variable
	 *
	 * @var string
	 **/
	public static $fallbackLocale = "en";

	/**
	 * Variable for loaded language files
	 *
	 * @var array
	 **/
	protected static $loaded = array();

	/**
	 * Construct Method
	 *
	 * @param array $paths File Paths
	 * @param array $locale Locales to loaded [default, fallback]
	 * @param array $options
	 */
	public function __construct($paths, $locale, $options = array())
	{
		#TODO
	}


	public function load($resource, $locale = "en")
	{
		if(false !== strpos($resource, "::"))
		{

		}

		if(file_exists(CORES.'Lang'.DS.$locale.DS.$resources.EXT))
		{

		}
	}

	public function get($key, $locale)
	{

	}

} // END class File Loader
