<?php

namespace Reborn\Cores\Translate;

/**
 * Translate Loader Interface is implement for all translate file loader
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
interface LoaderInterface
{
	public function load($resource, $locale);

	public function get($key, $locale);

} // END interface LoaderInterface
