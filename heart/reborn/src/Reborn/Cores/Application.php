<?php

namespace Reborn\Cores;

use Symfony\Component\HttpFoundation\Request as BaseRequest;
use Symfony\Component\HttpFoundation\Response as BaseResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
//use Symfony\Component\HttpKernel\HttpKernelInterface as HttpKernelInterface;

/**
 * Main Application Class for Reborn CMS
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class Application extends \Pimple
{

    /**
     * Marking for application started or not
     *
     * @var bool
     **/
    protected $started = false;

    /**
     * Constructor Method
     * Create new object for Reborn Application
     *
     * @return void
     **/
    public function __construct()
    {
        $this['request'] = Request::createFromGlobals();

        $this['response'] = Response::create('');

        $this['router'] =  $this->share(function ($this) {
            return new Router($this);
        });

        /*$this['input'] = $this->share(function ($this) {
            return new Input($this);
        });*/

        $this['log'] = $this->share(function () {
            return new Log();
        });

        $this['view'] = $this->share( function() {
            return new View();
        });

        $controller_container = new ControllerContainer();
        $this['controller.container'] = $controller_container->creator($this);
    }

    /**
     * Start the application
     * Make decision for Request, Response, Config
     *
     * @return void
     **/
    public function start()
    {
        if($this->started)
        {
            throw new \RbException("Reborn CMS Application is already started!");
        }

        // Set Exception and Error Handler
        $this->setExceptionHandler();
        $this->setErrorHandler();

        // call the appInitialize method
        $this->appInitialize();

        $response = $this['router']->dispatch();

        if(! $response instanceof Response)
        {
            $response = new Response($response);
        }

        //dump($response);

        $this->started = true;

        // Send response to the end method
        $this->end($response);
    }

    /**
     * Start the Initialize method from require classes.
     * But this method is call from application start method only.
     * Don't call more than once.
     *
     */
    public function appInitialize()
    {
        if($this->started)
        {
            return true;
        }

        // Start the Database initialize
        DB::initialize();

        // Start the Input Class
        Input::create($this);

        // Start the Event initialize
        Event::initialize();

        // Start the Setting initialize
        Setting::initialize();

        // Start the Module initialize
        Module::initialize();
    }

    /**
     * End point of application
     *
     * @return void
     **/
    public function end(Response $response)
    {
        $response->prepare($this['request']);

        if($response->getContent() == '')
        {
            // Set content is empty
            $response->setStatusCode(204);
        }

        if($response->isNotModified($this['request']))
        {
            //dump($response->getStatusCode());
        }
        else
        {
           // dump($response->isNotModified($this['request']));
        }

        $time = number_format((microtime(true) - REBORN_START_TIME) * 1000, 4)."ms";
        $mem = memory_get_peak_usage() - REBORN_START_MEMORY;
        $mem = (round($mem / pow(1024, 2), 3)."MB");

        $profiler = "<p>$time $mem</p>";

        $response->setContent($response->getContent().$profiler);

        return $response->send();
    }

    /**
     * Set Locale for application
     * Default locale is en
     *
     * @param string $locale
     * @return void
     **/
    public function setLocale($locale = 'en')
    {
        $this['locale'] = $locale;
    }

    /**
     * undocumented function
     *
     * @return void
     **/
    public function setExceptionHandler()
    {
        //$exp = new RbException(\Exception $e);
        //set_exception_handler(array($exp, "getStaticException"));
    }

    /**
     * Set the Fatal Error Handler
     *
     * @return void
     */
    public function setErrorHandler()
    {
        set_error_handler(array($this, "rbErrorHandler"));
    }

    public function rbErrorHandler($errno, $errstr, $errfile, $errline)
    {
        if (!(error_reporting() & $errno))
        {
            // This error code is not included in error_reporting
            return;
        }

        $style = <<<STYLE
        <style>
            p.php_error {
                background:#f9f9f9;
                color:#d00;
                border:1px solid #aaa;
                padding:10px;
                margin:15px;
                box-shadow: 2px 2px 2px #ccc;
                border-radius: 4px;
            }
            span.err_var {
                font-weight: bold;
            }
        </style>
STYLE;
        echo $style;

        echo '<p class="php_error">';
        switch ($errno) {
            case E_USER_ERROR:
                echo "<b>My ERROR</b> [$errno] $errstr<br />\n";
                echo "  Fatal error on line $errline in file $errfile";
                echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
                echo "Aborting...<br />\n";
                exit(1);
                break;

            case E_USER_WARNING:
                echo "<b>My WARNING</b> [$errno] $errstr<br />\n";
                break;

            case E_USER_NOTICE:
                echo "<b>My NOTICE</b> [$errno] $errstr<br />\n";
                break;

            default:
                echo "Unknown error type: [$errno] { $errstr } at line";
                echo " <span class='err_var';>$errline</span>";
                echo " from <span class='err_var';>$errfile</span><br />\n";
                break;
            }

        echo '</p>';

            /* Don't execute PHP internal error handler */
            return true;

    }

    /**
     * Magic setter method
     *
     * @param string $key
     * @param mixed $value
     * @return void
     **/
    public function __set($key, $value)
    {
        $this[$key] = $value;
    }

    /**
     * Magic getter method
     *
     * @param string $key
     * @return mixed
     **/
    public function __get($key)
    {
        return isset($this[$key]) ? $this[$key] : null;
    }

} // END class Application
