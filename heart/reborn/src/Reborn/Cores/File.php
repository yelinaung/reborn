<?php

namespace Reborn\Cores;

/**
 * File Class for Reborn CMS
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/

class FileException extends \Exception {};

class IOException extends \Exception {};

class File
{

	/**
	 * Get Content from Given File (file_get_contents)
	 *
	 * @param array $path File's path
	 * @return string
	 */
	public static function getContent($path)
	{
		if(! file_exists($path))
		{
			throw new FileException(sprintf("File not found at \"%s\"", $path));

		}

		return file_get_contents($path);
	}

	/**
	 * Get content from Remote Host
	 *
	 * @param array $path File's path
	 * @return mixed
	 */
	public static function getFromRemote($path)
	{
		return file_get_contents($path);
	}

	/**
	 * Write new file with given contents.
	 * Note : If file is doesn't already in given path, auto create this file.
	 *
	 * @param string $path Path of file locate
	 * @param string $filename File name to save or create.
	 * @param string $content Contents for file.
	 * @return void
	 */
	public static function write($path, $filename, $content = null)
	{
		#TODO
		$path = trim(str_replace(array('\\','/'), DS, $path));

		$file = $path.DS.$filename;

		if(file_exists($file))
		{
			$handle = fopen($path.DS.$filename, "w");
		}
		else
		{
			$handle = fopen($path.DS.$filename, "c");
		}

		if(! $handle)
		{
			throw new IOException(sprintf("Cannot open file '%s'", $file));
		}

		if(! fwrite($handle, $content))
		{
			throw new IOException(sprintf("Cannot write to file '%s'", $file));
		}

		fclose($handle);
	}

	public static function put($path, $data, $append = false)
	{
		if($append)
		{
			return file_put_contents($path, $data, FILE_APPEND | LOCK_EX);
		}

		return file_put_contents($path, $data, LOCK_EX);
	}

	public static function delete($path)
	{
		if(! file_exists($path)) return false;

		return unlink($path);
	}

} // END class File
