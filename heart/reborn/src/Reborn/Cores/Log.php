<?php

namespace Reborn\Cores;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

/**
 * Log Class for Reborn CMS
 *
 * This class is really adapter only between Monolog and Reborn CMS.
 * Supported logging methods are -
 *  -- debug
 *  -- info
 *  -- notice
 *  -- warning
 *  -- error
 *  -- critical
 *  -- alert
 *  -- emergency
 * You can see defination for above methods at monolog documentation.
 *
 * @package cores
 * @author Reborn CMS Development Team
 **/
class Log
{

	/**
	 * Variable for Log config items
	 *
	 * @var array
	 **/
	public $configs = array();

	#public $instance = null;

	/**
	 * Variable for monolog object
	 *
	 * @var object
	 **/
	public $logger = null;

	/**
	 * Default constructor method for log object
	 * You can pass configs values shch as
	 *
	 * <code>
	 * array(
	 * 	'path' => 'public/storages/applogs/',
	 * 	'file_name' => 'mylog-'Date(Y-m-d),
	 * 	'ext' => '.txt'
	 * );
	 * </code>
	 *
	 * @param string $name
	 * @param array $configs
	 * @return void
	 **/
	public function __construct($name = 'rebornCMSLog', $configs = array())
	{
		$defaultConfigs = Config::get('app.log');

		// Merge Default configs and given configs
		$this->configs = array_merge($defaultConfigs, $configs);

		$this->logger = new Logger($name);

		$fullpath = $this->configs['path'].$this->configs['file_name'].$this->configs['ext'];

		$this->logger->pushHandler(new StreamHandler($fullpath, Logger::DEBUG));
	}

	/**
	 * Get the Logger Object
	 *
	 * @return object
	 */
	public function getLogger()
	{
		return $this->logger;
	}

	/**
	 * Debug Log method adpater
	 *
	 * @param mixed $text Message you want to logging
	 */
	public function debug($text)
	{
		$this->logger->addDebug($text);
	}

	/**
	 * Info Log method adpater
	 *
	 * @param mixed $text Message you want to logging
	 */
	public function info($text)
	{
		$this->logger->addInfo($text);
	}

	/**
	 * Notice Log method adpater
	 *
	 * @param mixed $text Message you want to logging
	 */
	public function notice($text)
	{
		$this->logger->addNotice($text);
	}

	/**
	 * Warning Log method adpater
	 *
	 * @param mixed $text Message you want to logging
	 */
	public function warning($text)
	{
		$this->logger->addWarning($text);
	}

	/**
	 * Error Log method adpater
	 *
	 * @param mixed $text Message you want to logging
	 */
	public function error($text)
	{
		$this->logger->addError($text);
	}

	/**
	 * Critical Log method adpater
	 *
	 * @param mixed $text Message you want to logging
	 */
	public function critical($text)
	{
		$this->logger->addCritical($text);
	}

	/**
	 * Alert Log method adpater
	 *
	 * @param mixed $text Message you want to logging
	 */
	public function alert($text)
	{
		$this->logger->addAlert($text);
	}

	/**
	 * Emergency Log method adpater
	 *
	 * @param mixed $text Message you want to logging
	 */
	public function emergency($text)
	{
		$this->logger->addEmergency($text);
	}

} // END class Log
