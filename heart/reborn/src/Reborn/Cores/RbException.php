<?php

namespace Reborn\Cores;

use Reborn\Cores\Exception\RbExceptionInterface as RbExceptionInterface;

/**
 * RbException class. Extend the default Exception class
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class RbException extends \RuntimeException implements RbExceptionInterface
{

    public $handler = null;

    public function __construct($message, $code=NULL)
    {
        parent::__construct($message, $code);
        //$this->setHandler();
        $this->handler = get_called_class();
       // dump($this->handler);
    }

    public function setHandler()
    {
        $this->handler = get_class();
    }

    public function __toString()
    {
    	$message = $this->getMessage();
		$code = $this->getCode();
		$file = $this->getFile();
		$line = $this->getLine();
		$trace = $this->getTraceAsString();
		$date = date('M d, Y h:iA');
		//$exceptionHandler = str_replace(__NAMESPACE__.'\\', '', __CLASS__);

    	$output = '<div style="border: 1px solid #990000; padding: 5px; background:#EFBDBD; margin: 20px auto; width: 980px;">';
    	$output .= '<h3 style="color: #665151; font-size:16px;padding-bottom: 10px; margin-bottom: 10px; border-bottom: 1px dashed #f00;">';
    	$output .= '[ '.get_called_class(); //$this->handler;
        $output .= ' ] :: Exception Information for Reborn CMS.</h3>';
    	$output .= "<p><strong>Date: </strong>" . $date.'</p>';
        $output .= "<p><strong>Code no: </strong>" . $code.'</p>';
        $output .= "<p><strong>Message: </strong>" .$message.'</p>';
        $output .= "<p><strong>Line no: </strong>" . $line.'</p>';
        $output .= "<p><strong>File: </strong>" . $file.'</p>';

        //$output .= '<div style="border: 1px solid #990000; padding: 5px; background:#EFBDBD; margin: 10px;">';

        $output .= '<pre style="border: 1px solid #ee0000; padding: 5px; background:#FFFFFF; margin: 10px;">';
        $output .= $this->getTraceAsString().'</pre>';
        $output .= '</div>';

        return  $output;
    }

    public function getException(\Exception $e)
    {
        print new static($e->getMessage()); //$this; // This will print the return from the above method __toString()
    }

    public static function getStaticException(\Exception $e)
    {
        $handler = new static($e->getMessage());
        $handler->getException(); // $exception is an instance of this class
    }


} // END class Exception extends \Exception
