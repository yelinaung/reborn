<?php

namespace Reborn\Cores;

/**
 * HTTP Request Class
 *
 * @package cores
 * @author Reborn CMS Development Team
 **/
class Request extends \Symfony\Component\HttpFoundation\Request
{
	/**
	 * Check the request is Ajax or not
	 *
	 * @return bool
	 **/
	public function isAjax()
	{
		return $this->isXmlHttpRequest();
	}

	/**
	 * Get the Base URL
	 *
	 * @return string
	 */
	public function baseUrl()
	{
		return $this->getSchemeAndHttpHost().$this->getBasePath().'/';
	}
} // END Request class
