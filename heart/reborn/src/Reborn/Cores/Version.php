<?php

namespace Reborn\Cores;

/**
 * Version Control Class for Reborn CMS
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class Version
{
	const NAME = 'Reborn CMS';

	const FULL = '2.0.0-dev';

	const MAJOR = '2';

	const MINOR = '0';

	const FIX = '0';

	const EXTRA = 'dev';

	const CODE_NAME = 'rorb';

	/**
	 * Compare the Given Version and Current Version of Reborn CMS.
	 *
	 * @param string $version Version of the given to compare with current version.
	 * @return int Return is same with version_compare() function from PHP.
	 */
	public static function compare($new_version)
	{
		$currentVersion = str_replace(' ', '', self::FULL);
		$newVersion = str_replace(' ', '', $new_version);

		return version_compare($newVersion, $currentVersion);
	}

	/*public static function check()
	{
		$server_verison = File::getRemote('http://www.reborncms.com/check/version?current=1.0.0-beta');

		if($server_verison['status'] == 'needUpdate')
		{
			static::update($server_verison);
		}
	}

	public static function update($data = array())
	{
		$file_host = $data['fileHost'];


	}*/
} // END class Version
