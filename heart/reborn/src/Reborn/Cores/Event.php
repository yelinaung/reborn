<?php

namespace Reborn\Cores;

use Reborn\Cores\Exception\EventException as EventException;

/**
 * Event Class for Reborn CMS
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class Event
{

	/**
	 * Variable for events list
	 *
	 * @var array
	 */
	protected static $callbackEvents = array();

	/**
	 * Variable for callback method is not callable message
	 *
	 * @var string
	 */
	protected static $msg = 'Not callbable method { %s } for event name { %s } !';

	/**
	 * Variable for Log object
	 *
	 * @var object
	 */
	protected static $log;

	/**
	 * Initialize method for Event Class
	 *
	 * @param array $events Events array list
	 */
	public static function initialize($events = array())
	{
		static::$log = new Log;

		require CORES.'Event'.DS.'register'.EXT;

		//require CORES.'Config'.DS.'events'.EXT;

		if(! empty($events))
		{
			foreach($events as $k => $e)
			{
				static::add($e['name'], $e['callback']);
			}
		}
	}

	/**
	 * Add(Register) the Event.
	 * Don't use (.)dot operator at event name.
	 * If callback function is not callable, you can check log file.
	 * If you are developement process.
	 * You should set true for event.callback_throw in app config file.
	 * event.callback_throw is true, throw error msg when callback is not callable.
	 *
	 * @param string $name Event name (eg: blog_post_create)
	 * @param string $callback Callback function name.
	 */
	public static function add($name, $callback)
	{
		if(strpos($callback, '::'))
		{
			list($ns, $method) = explode('::', $callback);

			if(! is_callable(array($ns, $method)))
			{
				$msg = sprintf(static::$msg, $callback, $name);

				static::$log->error($msg);

				if(Config::get('app.event.callback_throw'))
				{
					throw new EventException($msg);
				}
			}
		}
		else
		{
			if(! is_callable($callback))
			{
		    	$msg = sprintf(static::$msg, $callback, $name);

				static::$log->error($msg);

		    	if(Config::get('app.event.callback_throw'))
				{
					throw new EventException($msg);
				}
		    }
		}

	    $eventName = strtolower($name);

	    static::$callbackEvents[$eventName][] = $callback;
	}

	/**
	 * Call(Trigger) the event.
	 *
	 * @param string $name Name of event
	 * @param array $data Data array for callback event (optional)
	 * @return void
	 */
	public static function call($name, $data = array())
	{
		$eventName = strtolower($name);

		if(isset(static::$callbackEvents[$eventName]))
		{
			foreach(static::$callbackEvents[$eventName] as $callback)
			{
				call_user_func_array($callback, array($data));
			}
		}
	}

	/**
	 * Check the given event name is have or not
	 *
	 * @param string $name Event name
	 * @return boolean
	 */
	public static function has($name)
	{
		$eventName = strtolower($name);

		return isset(static::$callbackEvents[$eventName]);
	}

	/**
	 * Remove(UnRegister) the given event name.
	 * Each event is an array.
	 * If you give name only, remove the all method from event name.
	 * If you want to remove event's some method from event name. use(.)
	 * eg ( eventname.method)
	 *
	 * @param string $name Name of the event
	 * @return void
	 */
	public static function remove($name)
	{
		$method = '';
		if(strpos($name, '.'))
		{
			list($name, $method) = explode('.', $name);
		}

		$eventName = strtolower($name);

		if(static::has($name))
		{
			if($method == '')
			{
				unset(static::$callbackEvents[$eventName]);
			}
			else
			{
				foreach(static::$callbackEvents[$eventName] as $k => $n)
				{
					if($method == $n)
					{
						unset(static::$callbackEvents[$eventName][$k]);
					}
				}
			}
		}
	}

	public static function getAll()
	{
		return static::$callbackEvents;
	}

} // END class Event
