<?php

namespace Reborn\Cores;

/**
 * Input Class for Reborn CMS
 *
 * You can use to pick up HTTP Request ($_GET, $_POST, $_SERVER).
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class Input
{

	/**
	 * Object Instance for Input Class
	 *
	 * @var object
	 */
	protected static $instance = null;

	/**
	 * App's static value
	 */
	protected static $app = null;

	/**
	 * Default constructor method.
	 *
	 */
	public function __construct() {}

	/**
	 * Create new Instance for this object
	 *
	 * @param array $app App obj from Application Class. But we need ['request'] only
	 */
	public static function create($app)
	{
		if(is_null(static::$instance))
		{
			static::$app = $app;
			static::$instance = true;
		}
	}

	/**
	 * Get request method is POST or GET
	 *
	 * @return string
	 **/
	public static function method()
	{
		return static::$app->request->getMethod();
	}

	/**
	 * Check the request method is POST
	 *
	 * @return boolean
	 */
	public static function isPost()
	{
		return ('POST' == static::$app->request->getMethod());
	}

	/**
	 * Get request param from $_GET (or) $_POST.
	 * $_GET (or) $_POST is base on request method.
	 *
	 * @param string $key
	 * @param mixed $default Default value if request key is not set
	 * @return mixed
	 */
	public static function get($key, $default = null)
	{
		return static::getByInputMethod($key, $default);
	}

	/**
	 * Get the all input field.
	 *
	 * @return array
	 **/
	public static function all()
	{
		$r = array();
		if(static::isPost())
		{
			$results = static::$app->request->request;

			foreach($results as $k => $v)
			{
				$r[$k] = $v;
			}

			return $r;
		}

		$results = static::$app->request->query;

		foreach($results as $k => $v)
		{
			$r[$k] = $v;
		}

		return $r;
	}

	/**
	 * Get request file information. Same with ($_FILES).
	 *
	 * @param string $key
	 * @param mixed $default Default value if request key is not set
	 * @return mixed
	 **/
	public static function file($key, $default = null)
	{
		return static::$app->request->files->get($key, $default);
	}

	/**
	 * Get request server information. Same with ($_SERVER).
	 * <code>
	 * 		// will return "localhost" at local server
	 * 		echo Input::server('HTTP_HOST');
	 * 		// OR
	 * 		echo Input::server('http_host');
	 * </code>
	 *
	 * @param string $key
	 * @param mixed $default Default value if request key is not set
	 * @return mixed
	 **/
	public static function server($key, $default = null)
	{
		$key = strtoupper($key);
		return static::$app->request->server->get($key, $default);
	}

	/**
	 * This method decide ($request[$_POST] or $query[$_GET]) base on request method
	 *
	 * @param string $key
	 * @param mixed $default Default value if request key is not set
	 * @return mixed
	 */
	protected static function getByInputMethod($key, $default = null)
	{
		if(static::$app->request->getMethod() == 'POST')
		{
			return static::$app->request->request->get($key, $default);
		}
		else
		{
			return static::$app->request->query->get($key, $default);
		}
	}
}
