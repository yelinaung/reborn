<?php

namespace Reborn\Cores;

/**
 * Class Alias Library for Reborn CMS
 *
 * @package default
 * @author Reborn CMS Development Team
 **/
class Alias
{

	/**
	 * Marking for Reborn's Core Class are alias or not
	 *
	 * @var bool
	 **/
	protected static $coreIsAlias = false;

	/**
	 * Array list for Reborn's Core Class
	 *
	 * @var array
	 **/
	protected static $coreClasses = array(
			'ArrayMethods' => 'Reborn\Cores\Methods_ArrayMethods',
			'Config' => 'Reborn\Cores\Config',
			'Controller' => 'Reborn\Cores\Controller',
			'ControllerContainer' => 'Reborn\Cores\ControllerContainer',
			'DB' => 'Reborn\Cores\DB',
			'Eloquent' => 'Illuminate\Database\Eloquent\Model',
			'Event' => 'Reborn\Cores\Event',
			'RbException' => 'Reborn\Cores\RbException',
			'Redirect' => 'Reborn\Cores\Redirect',
			'Form' => 'Reborn\Cores\Form',
            'Input' => 'Reborn\Cores\Input',
            'Inspector' => 'Reborn\Cores\Inspector',
            'Log' => 'Reborn\Cores\Log',
            'Module' => 'Reborn\Cores\Module',
            'Route' => 'Reborn\Cores\Route',
            'Router' => 'Reborn\Cores\Router',
            'Model' => 'Reborn\Cores\Model',
            'Schema' => 'Reborn\Cores\DB\Schema',
            'Setting' => 'Reborn\Cores\Setting',
            //'Template' => 'Reborn\Cores\Template',
            'Uri' => 'Reborn\Cores\Uri',
            //'View' => 'Reborn\Cores\View',
		);

	/**
	 * Construct method is final, don't allown to override these method.
	 *
	 */
	final function __construct() {}

	/**
	 * Class Alias for Reborn's Cores Class.
	 * This method is call when application started.
	 * But not allow to call after one time.
	 *
	 * @return void
	 */
	public static function coreClassAlias()
	{
		if(! static::$coreIsAlias)
		{
			static::aliasRegister(static::$coreClasses);

			static::$coreIsAlias = true;
		}
	}

	/**
     * Register the class alias.
     * You can use your class (enternal of Reborn's Cores) to alies use this method.
     * <code>
     * 		// If you want to use Products\Libs\Shopper to Shopper
     * 		Alias::aliasRegister(array('Shopper' => 'Products\Libs\Shopper'));
     *
     * 		// Ok, Shopper class is aliased now
     * 		Shopper::calculate();
     * </code>
     *
     * @param array $classes Array data for class alias
     * @return void
     */
    public static function aliasRegister($classes = array())
    {
        foreach($classes as $alias => $class)
        {
            class_alias($class, $alias);
        }
    }

} // END class Alias
