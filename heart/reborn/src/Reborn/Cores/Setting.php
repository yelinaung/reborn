<?php

namespace Reborn\Cores;

/**
 * Setting Class for Reborn CMS.
 * Setting from db_setting table.
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class Setting
{
	/**
	 * Setting items array
	 *
	 * @var array
	 **/
	protected static $items = array();

	/**
	 * Variable for Setting table
	 *
	 * @var string
	 **/
	protected static $_table = 'settings';

	/**
	 * Marker for initialize function
	 *
	 * @var boolean
	 */
	protected static $started = false;

	/**
	 * Initialize method for setting class
	 *
	 */
	public static function initialize()
	{
		if(static::$started)
		{
			return true;
		}
		else
		{
			static::findAllFromDB();
		}
	}

	/**
	 * Get the setting item with key name (slug Column in db table).
	 *
	 * @param string $key Key(slug) name for setting item
	 * @return mixed If item not found, return null.
	 */
	public static function get($key)
	{
		if(isset(static::$items[$key]))
		{
			return static::$items[$key];
		}
		else
		{
			return null;
		}
	}

	public static function set($key, $value)
	{
		if(isset(static::$items[$key]))
		{
			$update = DB::table(static::$_table)->where('slug', '=', $key)
									->update(array('value' => $value));
			if($update)
			{
				static::$items[$key] = $value;
			}
		}
	}

	/**
	 * Check given key's real value is same with given value.
	 * example:
	 * <code>
	 * 		// setting item site_title is "My Site" in the DB
	 * 		Setting::is('site_title', "My Site"); // return true;
	 * 		Setting::is('site_title', "Hello World"); // return false;
	 * </code>
	 *
	 * @param string $key Name of the setting item
	 * @param mixed $thinkValue Think value for given setting item name
	 * @return boolean
	 */
	public static function is($key, $thinkValue)
	{
		$realValue = static::get($key);

		return ($thinkValue == $realValue);
	}

	/**
	 * Find all items from DB table.
	 *
	 * @return void
	 **/
	private static function findAllFromDB()
	{
		$items = DB::table(static::$_table)->get();

		foreach($items as $i)
		{
			static::$items[$i->slug] = ($i->value !== '') ? $i->value : $i->default;
		}
	}

} // END class Setting
