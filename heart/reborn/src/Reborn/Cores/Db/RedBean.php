<?php

namespace Reborn\Cores;

$file = realpath(SYSTEM.'vendor/gabordemooij/redbean/RedBean/');
require_once($file.DS.'redbean.inc.php');

/**
 * DB Class for Reborn CMS
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class Db_RedBean extends \R
{
	/**
	 * RB Connect Example
	 *
	 * R::setup('mysql:host=localhost;dbname=mydatabase', 'user','password'); //mysql
	 *
	 * R::setup('pgsql:host=localhost;dbname=mydatabase', 'user','password'); postgresql
	 *
	 *  R::setup('sqlite:/tmp/dbfile.txt', 'user','password'); //sqlite
	 */

	public static function initialize()
	{
		$hostname = Config::get('db.host');
		$database = Config::get('db.dbname');
		$user = Config::get('db.username');
		$password = Config::get('db.password');
		static::setup("mysql:host={$hostname};dbname={$database}", $user, $password);
	}

} // END class DB
