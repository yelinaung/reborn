<?php

namespace Reborn\Cores\DB;

use Reborn\Cores\DB as DB;

/**
 * Schema Class is Bridge between Reborn and Illuminate's Schema.
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class Schema
{
	public static function __callStatic($method, $params = array())
	{
		$args = $params;
		$schemaBuilder = DB::getSchemaBuilder();
		return call_user_func_array(array($schemaBuilder, $method), $args);
	}

} // END class Schema
