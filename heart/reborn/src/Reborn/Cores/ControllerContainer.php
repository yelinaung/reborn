<?php

namespace Reborn\Cores;

/**
 * Container class for Controller
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class ControllerContainer
{
	/**
	 * request object variable for controller
	 *
	 * @var Reborn\Cores\Request
	 **/
	protected $request = null;

	/**
	 * response object variable for controller
	 *
	 * @var Reborn\Cores\Response
	 **/
	protected $response = null;

	/**
	 * view object variable for controller
	 *
	 * @var Reborn\Cores\View
	 **/
	protected $view = null;

	/**
	 * Variavle for HTTP status code for response
	 *
	 * @var int
	 */
	protected $HTTPstatus = 200;

	/**
	 * Variable for ETag use or not. Default is false.
	 *
	 * @var boolean
	 */
	protected $Etag = false;

	public function __construct() {}

	public function before() {}

	/**
	 * After Method for Controller
	 * This method return the Response Object.
	 *
	 * @param mixed $response
	 */
	public function after($response)
	{
		if(! $response instanceof Response)
		{
			$response = new Response($response, $this->HTTPstatus);
		}

		if($this->Etag)
		{
			$response->setEtag(md5($response->getContent()));
		}

		return $response;
	}

	/**
	 * Create the Controller with Application Object
	 * This method is call from Application start only.
	 *
	 * @param Reborn\Cores\Application
	 * @return Reborn\Cores\Controller
	 */
	final function creator(Application $app)
	{
		$this->response = $app['response'];
		//$this->request = $app['request'];
		//$this->view = $app['view'];
		//$this->template = $app['tempalte'];
	}

	/**
	 * Set the HTTP status code to use at response.
	 *
	 * @param int $code HTTP status code.
	 * @return void
	 */
	protected function setHTTPstatus($code)
	{
		$realCode = Response::$statusTexts;

		if(isset($realCode[$code]))
		{
			$this->HTTPstatus = $code;
		}
	}

} // END class ControllerContainer
