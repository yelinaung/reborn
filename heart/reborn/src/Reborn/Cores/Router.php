<?php

namespace Reborn\Cores;

/**
 * Router Class for Reborn CMS
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class Router
{

	/**
	 * Default module variable.
	 *
	 * @var string
	 **/
	protected $module= null;

	/**
	 * Default controller variable
	 *
	 * @var string
	 **/
	protected $controller = null;

	/**
	 * Default method variable
	 *
	 * @var string
	 **/
	protected $method = 'index';

	/**
	 * Variable for request params
	 *
	 * @var array
	 */
	protected $params = array();

	/**
	 * Variable for request object
	 *
	 * @var object
	 */
	protected $request;

	/**
	 * Constructor Method
	 *
	 * @return void
	 **/
	public function __construct($app)
	{
		$this->request = $app['request'];
		unset($app);

		Uri::initialize($this->request);

		require CORES.'Config'.DS.'routes'.EXT;
	}

	/**
	 * Get the active module.
	 *
	 * @return string
	 **/
	public function activeModule()
	{
		return $this->module;
	}

	/**
	 * Get the active controller.
	 *
	 * @return string
	 **/
	public function activeController()
	{
		return $this->controller;
	}

	/**
	 * Get the active method.
	 *
	 * @return string
	 **/
	public function activeMethod()
	{
		return $this->method;
	}

	/**
	 * Get the active method's params.
	 *
	 * @return array
	 **/
	public function params()
	{
		return $this->params;
	}

	/**
	 * Get the active method's param by name.
	 *
	 * @return mixed
	 **/
	public function param($key)
	{
		return isset($this->params[$key]) ? $this->params[$key] : null;
	}

	/**
	 * Dispatch the route for Reborn CMS
	 *
	 * @return void
	 */
	public function dispatch()
	{
		$uri = $this->request->getPathInfo();

		$uriArr = Uri::segments();

		$realModule = false;

		if(isset($uriArr[0]))
		{
			$realModule = $uriArr[0];
		}

		$moduleName = ($realModule) ? $realModule : null;

		$route = Route::get($uri, $moduleName);

		if(! is_null($route))
		{
			if(isset($route['method']) and $route['method'] instanceof \Closure)
			{
				return call_user_func_array($route['method'], $route['matches']);
			}

			if(isset($route['module']))
			{
				$this->module = strtolower($route['module']);
				unset($route['module']);
			}

			if(isset($route['controller']))
			{
				$this->controller = strtolower($route['controller']);
				unset($route['controller']);
			}

			if(isset($route['action']))
			{
				$this->method = strtolower($route['action']);
				unset($route['action']);
			}

			$this->params = $route;

			unset($route);
		}

		if( Module::isEnabled($this->module))
		{
			$class = '\\'.ucfirst($this->module).'\\Controller\\'.ucfirst($this->controller).'Controller';

			return $this->callbackController($class, $this->params);
		}
	}

	/**
	 * call the given controller's action(method).
	 *
	 * @param string $class Contoller class to call.
	 * @param string $method Method of given controller class.
	 * @return void
	 **/
	protected function callbackController($class, $params = array())
	{
		Module::load($this->module);

		$method = $this->method;

		if(class_exists($class))
		{
			$controllerClass = new $class($this->request);

			if(method_exists($controllerClass, $method))
			{
				// Call the Controller class's before method. Instead of __construct
				$controllerClass->before();

				$response = call_user_func_array(array($controllerClass, $method), $params);

				// Call the Controller class's after method. Instead of __destruct
				$response = $controllerClass->after($response);

				return $response;
			}
			else
			{
				return 'Need to assign HTTP 404 Result';
			}
		}
		else // We need 404
		{
			return 'Real class';
		}
	}

} // END class Router
