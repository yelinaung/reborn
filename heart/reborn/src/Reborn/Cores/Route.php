<?php

namespace Reborn\Cores;

/**
 * Route class
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class Route
{

	/**
	 * route list of Reborn CMS
	 *
	 * @var array
	 **/
	protected static $routes = array();

	/**
	 * Constants for Regex Pattern
	 */
	const REGEX_ANY = "([^/]+?)";
	const REGEX_INT = "([0-9]+?)";
	const REGEX_ALPHA = "([a-zA-Z\.\-_]+?)";
	const REGEX_ALPHANUMERIC = "([0-9a-zA-Z\.\-_]+?)";
	const REGEX_STATIC = "%s";

	/**
	 * Add a new route
	 *
	 * @param string $route The route pattern
	 * @param array $options Array of given route's options
	 * @return void
	 **/
	public static function add($route, $options = array())
	{
		static::$routes[$route] = array(
				'pattern' => static::parsePattern($route),
				'options' => $options
			);
	}

	/**
	 * Retrieve the route data
	 *
	 * @param string $request The request URI
	 * @param string $module Module name. This is use to pickup module's route file.
	 * @return array
	 */
	public static function get($request, $module = null)
	{
		$matches = array();

		// If the given module is not null, we set default module from setting
		if(is_null($module))
		{
			$module = Setting::get('default_module');
		}


		// We find routes.php file from module config folder
		// If file exits, import this file
		if(Module::isEnabled($module))
		{
			Module::load($module);
			if(Module::isCore($module))
			{
				$modRouteFile = CORE_MODULES.$module.DS.'config'.DS.'routes'.EXT;
			}
			else
			{
				$modRouteFile = MODULES.$module.DS.'config'.DS.'routes'.EXT;
			}

			if(file_exists($modRouteFile))
			{
				require $modRouteFile;
			}
		}

		foreach (static::$routes as $route)
		{
			// Try to match the request against defined routes
			if (preg_match($route['pattern'], $request, $matches))
			{
				// If it matches, remove unnecessary numeric indexes
				foreach ($matches as $key => $value)
				{
					if (is_int($key)) {
						unset($matches[$key]);
					}
				}

				// Check options is closure function
				if($route['options'] instanceof \Closure)
				{
					if(is_callable($route['options']))
					{
						$result['method'] = $route['options'];
						$result['matches'] = $matches;

						return $result;
					}
					else
					{
						throw new \Exception("Request function is wrong.");

					}
				}

				// Merge the matches with the supplied options
				$result = $matches + $route['options'];

				return $result;
			}
			else
			{

			}
		}

    	throw new \Exception("Route not found");
	}

	protected static function parsePattern($route)
	{
		// empty URI
		if ($route == '/') {
			return "#^/$#";
		}

		$parts = explode("/", $route);

		// Start with our base URL
		$regex = "#^";

		if ($route[0] == "/") {
			array_shift($parts);
		}

		foreach ($parts as $part) {

			$regex .= "/";

			$args = explode(":", $part);

			if (sizeof($args) == 1)
			{

				$regex .= sprintf(self::REGEX_STATIC, preg_quote(array_shift($args), '#'));
				continue;
			}
			elseif ($args[0] == '{')
			{
				array_shift($args);
				$type = false;
			}
			else
			{
				$type = ltrim(array_shift($args),'{');
			}

			$key = rtrim(array_shift($args),'}');

			if ($type == "regex") {
				$regex .= $key;
				continue;
			}

			static::normalize($key);

			$regex .= '(?P<' . $key . '>';

			switch (strtolower($type)) {
				case "int":
					$regex .= self::REGEX_INT;
					break;
				case "alpha":
					$regex .= self::REGEX_ALPHA;
					break;
				case "alnum":
					$regex .= self::REGEX_ALPHANUMERIC;
					break;
				default:
					$regex .= self::REGEX_ANY;
					break;
			}

			$regex .= ")";
		}

		$regex .= '$#u';

		return $regex;
	}

	/**
	 * Normalize a string for sub-pattern naming
	 *
	 * @param string &$param
	 */
	public static function normalize(&$param)
	{
		$param = preg_replace("/[^a-zA-Z0-9]/", "", $param);
	}


} // END class Route
