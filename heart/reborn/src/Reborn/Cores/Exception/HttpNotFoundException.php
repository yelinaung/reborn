<?php

namespace Reborn\Cores\Exception;

class FileNotFoundException extends \Reborn\Cores\RbException
{

	public function __construct($message, $code=NULL)
    {
        parent::__construct($message, $code);
        $this->handler = get_class();
    }

}
