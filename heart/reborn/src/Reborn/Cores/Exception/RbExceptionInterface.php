<?php

namespace Reborn\Cores\Exception;

interface RbExceptionInterface
{

	/**
	 * Get the Current Exception Handler Name
	 * This name is show in Exception Output Title
	 * <code>
	 * 		public function setHandler()
	 * 		{
	 * 			$this->handler = get_class();
	 * 		}
	 * </code>
	 */
    //public function setHandler();

}
