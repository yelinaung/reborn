<?php

namespace Reborn\Cores\Exception;

use \Reborn\Cores\RbException as RbException;
use Reborn\Cores\Exception\RbExceptionInterface as RbExceptionInterface;

class EventException extends RbException implements RbExceptionInterface
{
    public function setHandler()
    {
    	$this->handler = 'EventException';
    }

}
