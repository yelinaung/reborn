<?php

namespace Reborn\Cores\Exception;

use \Reborn\Cores\RbException as RbException;
use Reborn\Cores\Exception\RbExceptionInterface as RbExceptionInterface;

class ModuleException extends RbException implements RbExceptionInterface
{

	public function __construct($msg, $code=NULL)
    {
        //$message = sprintf("Module name { %s } doesn't exits in module path.", $file);
        parent::__construct($msg, $code);
    }

    public function setHandler()
    {
    	$this->handler = 'ModuleNotFoundException';
    }

}
