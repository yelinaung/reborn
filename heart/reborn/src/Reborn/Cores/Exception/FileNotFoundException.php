<?php

namespace Reborn\Cores\Exception;

use \Reborn\Cores\RbException as RbException;
use Reborn\Cores\Exception\RbExceptionInterface as RbExceptionInterface;

class FileNotFoundException extends RbException implements RbExceptionInterface
{

	public function __construct($file, $path, $code=NULL)
    {
        $message = sprintf("%s file doesn't exits in given %s.", $file, $path);
        parent::__construct($message, $code);
    }

    public function setHandler()
    {
    	$this->handler = 'FileNotFoundException';
    }

}
