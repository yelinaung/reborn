<?php

namespace Reborn\Cores\Module;

/**
 * Module Initialize Intreface
 *
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
Interface InitializeInterface
{
	/**
	 * This method is call when module installation stage.
	 * If you need create new database or other case at module installation stage,
	 * You should crate at this method.
	 *
	 * @return void
	 **/
	public function install();

	/**
	 * This method is call when module uninstallation stage.
	 * If you need delete new database or other case at module uninstallation stage,
	 * You should crate at this method.
	 *
	 * @return void
	 **/
	public function uninstall();

	/**
	 * This method is call when module upgrade stage.
	 * If you need change within older module version (from DB) to current verison,
	 * You should crate at this method.
	 *
	 * @param string $dbVersion Version no: from the module DB table
	 * @return void
	 **/
	public function upgrade($dbVersion);

	/**
	 * Boot method for module.
	 * This method is call when module load stage.
	 * Example :
	 * <code>
	 * 		public function boot()
	 * 		{
	 * 			// I load the translate file at here.
	 * 			Translate::load('blog::formlabel');
	 * 			Translate::load('blog::validation');
	 *
	 * 			// Register the event for this module
	 *			Event::add('blog_post_create', function($data){
	 * 				$title = $data['title'];
	 * 				$author = $data['author'];
	 * 				Log::info("$author create new post \"$title\"");
	 * 			});
	 * 		}
	 * </code>
	 *
	 * @return void
	 */
	public function boot();

	/**
	 * Shutdown method for module.
	 * This method is call when module unload stage.
	 * Example :
	 * <code>
	 * 		public function shutdown()
	 * 		{
	 * 			// Register the event for this module
	 *			Event::remove('blog_post_create');
	 * 		}
	 * </code>
	 *
	 * @return void
	 */
	public function shutdown();

} // END Interface InitializeInterface
