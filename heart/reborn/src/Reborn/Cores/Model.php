<?php

namespace Reborn\Cores;

/**
 * Model Class for Reborn CMS
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class Model
{
	protected static $table = null;

	protected static $pk = 'id';

	public static function get($id)
	{
		$result = DB::table(static::$table)->where(static::$pk, '=', $id)->get();
		return (count($result) > 0) ? $result[0] : array();
	}

} // END class Model
