<?php

namespace Reborn\Cores;

/**
 * HTTP Response Class
 *
 * @package cores
 * @author Reborn CMS Development Team
 **/
class Response extends \Symfony\Component\HttpFoundation\Response
{
	/**
	 * Check the request is Ajax or not
	 *
	 * @return bool
	 **/
	public function isAjax()
	{
		return $this->isXmlHttpRequest();
	}
} // END Request class
