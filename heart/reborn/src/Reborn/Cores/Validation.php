<?php

namespace Reborn\Cores;

/**
 * Validation class for Reborn CMS #TODO - Need to add callback function for own rule
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class Validation
{
	/**
	 * Validation inputs attributes
	 *
	 * @var array
	 **/
	protected $inputs = array();

	/**
	 * Validation rules array by default
	 *
	 * @var array
	 **/
	protected $rules = array();

	/**
	 * Validation errors array
	 *
	 * @var array
	 **/
	protected $errors = array();

	/**
	 * Accepted validation method list by Default
	 *
	 * 01) - max [Input value's Maximum is given value]
	 * 02) - min [Input value's Minimum is given value]
	 * 03) - maxLength [Input value's string length Maximum is given value]
	 * 04) - minLength [Input value's string length Minimum is given value]
	 * 05) - required [Input value is required]
	 * 06) - alpha [Input value is Alpha(a-zA-Z)]
	 * 07) - alphaNum [Input value is AlphaNumeric(a-zA-Z0-9)]
	 * 08) - alphaDash [Input value is AlphaNumeric, Dash and Underscore(a-zA-Z0-9-_)]
	 * 09) - alphaDashDot [Input value is alphaDash and Dot(a-zA-Z0-9-_.)]
	 * 10) - numeric [Input value is Numeric(0-9)]
	 * 11) - integer [Input value is Integer value]
	 * 12) - email [Input value is Valid Email]
	 * 13) - url [Input value is Valid URL]
	 * 14) - ip [Input value is Valid IP address]
	 * 15) - between [Input value is Between first and last value eg:between:5,7]
	 * 16) - equal [Input valis is Equal with given value or field name eg:equal:9]
	 *
	 * @var array
	 **/
	protected $methods = array(
				'max',
				'min',
				'maxLength',
				'minLength',
				'required',
				'alpha',
				'alphaNum',
				'alphaDash',
				'alphaDashDot',
				'numeric',
				'integer',
				'email',
				'url',
				'ip',
				'between',
				'equal',
				'pattern'
			);

	/**
	 * New Method list (callback) added by user
	 *
	 * @var array
	 **/
	protected $addedMethods = array();

	/**
	 * Construct the Validation Class
	 * Example :
	 * <code>
	 * 		// Input field
	 * 		//eg: array('name' => 'Reborn', 'age' => '25', 'address' =>'Yangon-Myanmar')
	 * 		$inputs = array( // Get the all input from form submit
	 * 				'name' => Input::get('name'),
	 * 				'age' => Input::get('age'),
	 * 				'address' => Input::get('address')
	 * 			);
	 * 		$rules = array(
	 * 				'name' => 'required',
	 * 				'age' => 'between:18,30',
	 * 				'address' => 'minLength:10|alphaDashDot'
	 * 			);
	 * 		$v = new Validation($inputs, $rules);
	 * 		if($v->valid())
	 * 		{
	 * 			echo 'Input is Valid';
	 * 		}
	 * 		else
	 * 		{
	 * 			$errs = $v->getErrors();
	 * 			foreach($errs as $e)
	 * 			{
	 * 				echo '$e'.'<br>';
	 * 			}
	 * 		}
	 * </code>
	 *
	 * @param array $inputs Input fields
	 * @param array $rules Rules for Input field
	 * @return void
	 **/
	public function __construct($inputs = array(), $rules)
	{
		$this->inputs = $inputs;

		foreach($rules as $key => &$rule)
		{
			$rule = (is_string($rule)) ? explode('|', $rule) : $rule;
		}

		$this->rules = $rules;
	}

	/**
	 * Static Method for Validation Instance
	 *
	 * @param array $inputs Input fields
	 * @param array $rules Rules for Input field
	 * @return Reborn\Cores\Validation
	 **/
	public static function create($inputs = array(), $rules)
	{
		return new static($inputs, $rules);
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 **/
	public function addRule($name, $msg, $callback)
	{
		$this->addedMethods[$name]['call'] = $callback;
		$this->addedMethods[$name]['msg'] = $msg;

		dump($this->addedMethods);
	}

	/**
	 * Check the validation is valid or not.
	 *
	 * @return boolean
	 **/
	public function valid()
	{
		foreach($this->rules as $input => $rules)
		{
			if(is_array($rules))
			{
				foreach ($rules as $rule)
				{
					$this->compute($input, $rule);
				}
			}
			else
			{
				$this->compute($input, $rules);
			}
		}

		if(count($this->errors) == 0)
		{
			return true;
		}
		return false;
	}

	/**
	 * Opposite of valid method
	 *
	 * @return boolean
	 **/
	public function fail()
	{
		return !$this->valid();
	}

	/**
	 * Get the Validation Errors. If doesn't have any validation error, return null
	 *
	 * @return array|null
	 **/
	public function getErrors()
	{
		return (count($this->errors) !== 0) ? $this->errors : null;
	}

	/**
	 * Compute the Input and his rule is valid or not.
	 *
	 * @param string $input Input field name
	 * @param string $rule Validation rule for given input field
	 * @return void
	 **/
	protected function compute($input, $rule)
	{
		list($rule, $param) = $this->ruleParser($rule);

		if(in_array($rule, $this->methods))
		{
			// First param is Input value and second is Rule's value
			$args = array($this->inputs[$input], $param);

			if(! call_user_func_array(array($this, 'valid'.ucfirst($rule)), $args))
			{
				$this->setError($rule, $input, $param);
			}
		}
		elseif(array_key_exists($rule, $this->addedMethods))
		{
			// First param is Input value and second is Rule's value
			$args = array($this->inputs[$input], $param);
			$method = $this->addedMethods[$rule]['call'];
			if(! call_user_func_array($method, $args))
			{
				$this->setError($rule, $input, $param, true);
			}
		}
	}

	/**
	 * Parse the given rule string to rule and rule's parameter.
	 * Have a char ':' at given rule, parse the rule name and param.
	 * If doesn't have ':', return rule name and param is null.
	 *
	 * @param string $rule
	 * @return array
	 **/
	protected function ruleParser($rule)
	{
		if(false !== ($pos = strpos($rule, ':')))
		{
			return array(substr($rule, 0, $pos), substr($rule, $pos +1));
		}

		return array($rule, null);
	}

	/**
	 * set the validation error.
	 * This method is call from $this->compute() when the validation is invalid.
	 *
	 * @param string $rule Name of the validation rule
	 * @param string $key Input field name.
	 * @param string $arg Argumetn string for rule (eg: max,min etc:)
	 * @param boolean $addedRule This error from addedMethods or default Methods
	 * @return void
	 **/
	protected function setError($rule, $key, $arg, $addedRule = false)
	{
		if($addedRule)
		{
			$msg = $this->addedMethods[$rule]['msg'];
		}
		else
		{
			// Change this is Language file
			$msg = Config::get('valid.'.$rule);
		}

		$parseMsg = str_replace('{key}', $key, $msg);

		if('between' == $rule)
		{
			$args = explode(',', $arg);
			$parseMsg = str_replace(array('{first}', '{last}'), $args, $parseMsg);
		}
		else
		{
			$parseMsg = str_replace('{'.$rule.'}', $arg, $parseMsg);
		}

		$this->errors[$key] = $parseMsg;
	}


	/* =============== Validation Method Lists =================== */

	protected function validMax($value, $max)
	{
		return ($value <= (int)$max);
	}

	protected function validMin($value, $min)
	{
		return ($value >= (int)$min);
	}

	protected function validMaxLength($value, $max)
	{
		return (strlen($value) <= (int)$max);
	}

	protected function validMinLength($value, $min)
	{
		return (strlen($value) >= (int)$min);
	}

	protected function validRequired($value)
	{
		if(is_string($value) and trim($value) == '')
		{
			return false;
		}
		elseif(is_null($value))
		{
			return false;
		}

		return true;
	}

	protected function validAlpha($value)
	{
		return preg_match("#^([a-zA-Z]+)$#", $value);
	}

	protected function validAlphaNum($value)
	{
		return preg_match("#^([a-zA-Z0-9]+)$#", $value);
	}

	protected function validAlphaDash($value)
	{
		return preg_match("#^([a-zA-Z0-9_-]+)$#", $value);
	}

	protected function validAlphaDashDot($value)
	{
		return preg_match("#^([a-zA-Z0-9\.\_-]+)$#", $value);
	}

	protected function validNumeric($value)
	{
		return is_numeric($value);
	}

	protected function validInteger($value)
	{
		return filter_var($value, FILTER_VALIDATE_INT) !== false;
	}

	protected function validEmail($value)
	{
		return filter_var($value, FILTER_VALIDATE_EMAIL) !== false;
	}

	protected function validUrl($value)
	{
		return filter_var($value, FILTER_VALIDATE_URL) !== false;
	}

	protected function validIp($value)
	{
		return filter_var($value, FILTER_VALIDATE_IP) !== false;
	}

	protected function validBetween($value, $bet)
	{
		list($first, $last) = explode(',', $bet);

		return (((int)$first < (int)$value) and ((int)$value < (int)$last));
	}

	protected function validEqual($value, $eq)
	{
		if(isset($this->inputs[$eq]))
		{
			return ($value === $this->inputs[$eq]);
		}
		else
		{
			return ($value === $eq);
		}
	}

	protected function validPattern($value, $pattern)
	{
		return preg_match($pattern, $value);
	}


} // END class Validation
