<?php

/**
 * Helper Functions for Reborn CMS
 *
 * @package Reborn CMS
 * @author Reborn CMS Development Team
 **/

/**
 * Dump the given value.Use for var_dump().
 * If you want to die the script after dump, use as
 * <code>
 * dump($value, true);
 * </code>
 *
 * @param mixed $value
 * @param bool $die
 *
 */
if(! function_exists('dump'))
{
	function dump($value, $die = false)
	{
		$backtrace = debug_backtrace();
		$file = $backtrace[0]['file'];
		$line = $backtrace[0]['line'];

		echo '<pre style="border: 1px dashed #71CF4D; padding: 5px 10px; background: #F2F2F2; margin: 10px 15px;">';
		echo '<h3 style="color: #990000; margin-top: 0px; padding-bottom: 5px; border-bottom: 1px dashed #999; font-size: 13px; font-weight: normal;">';
		echo 'Dump at <span style="color: #000099;">'.$file.'</span> ';
		echo 'line number <span style="color: #009900;">'.$line.'</span>.</h3>';
		var_dump($value);
		echo '</pre>';
		if($die)
		{
			die;
		}
	}
}

/**
 * Return the value of given param,
 * If the value is closure, return closure result.
 *
 * @param mixed $val
 * @return mixed
 */
if(! function_exists('value'))
{
	function value($val)
	{
		return ($val instanceof \Closure) ? $val() : $val;
	}
}

/**
 * import php file from System's Cores Folder
 *
 * @return void
 **/
if(! function_exists('import'))
{
	function import($path, $folder = 'Cores')
	{
		$path = str_replace('/', DIRECTORY_SEPARATOR, $path);

		if (is_file(CORES.$folder.DIRECTORY_SEPARATOR.$path.EXT))
		{
			require_once CORES.$folder.DIRECTORY_SEPARATOR.$path.EXT;
		}
	}
}

/**
 * import php file from System's Cores Folder
 *
 * @return void
 **/
if(! function_exists('url'))
{
	function url($path)
	{
		dump($app);
	}
}
