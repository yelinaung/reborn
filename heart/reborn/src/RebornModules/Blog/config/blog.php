<?php

return array(
	'title' => 'Blog Title',
	'body' => 'Blog Body',
	'cat' => array(
			'test' => 'Test Cat',
			'sample' => array(
					'third' => 'Third Level Array Key',
					'tt' => array(
							'fourth' => 'I am Fourth Level'
						)
				),
		),
);
