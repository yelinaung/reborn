<?php

Route::add('blog/view/{any:filename}/{int:age}/{alnum:job}', array(
		'module' => 'blog',
		'controller' => 'blog',
		'action' => 'view'
	));

Route::add('blog/category/{any:name}', array(
		'module' => 'blog',
		'controller' => 'blog',
		'action' => 'category'
	));
