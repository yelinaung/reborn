<?php

namespace Blog\Installer;

use Reborn\Cores\Module\InitializeInterface as InitializeInterface;


/**
 * Reborn CMS's Core Module Pages.
 * This is initialize file for Pages Module. Required.
 * This class is implements the Reborn\Core\Module\InitializeInterface
 * Needed following 5 method.
 *  -- 1) install() [This method call when the module installation]
 *  -- 2) uninstall() [This method call when the module uninstallation]
 *  -- 3) upgrade() [This method call when the module is upgrade]
 *  -- 4) boot() [This method call when the module booted (load)]
 *  -- 5) shutdown() [This method call when the module shutdown(unLoaded)]
 *
 * @package Cores
 * @author Reborn CMS Development Team
 **/
class Initialize implements InitializeInterface
{
	public function install()
	{
		\Schema::table('blog', function($table)
		    {
			    $table->create();
			    $table->increments('id');
			    $table->string('title');
			    $table->text('content');
			    $table->string('author')->nullable();
			    $table->text('brief');
			    $table->timestamps();
		    });
	}

	public function uninstall()
	{
		\Schema::drop('blog');
	}

	public function upgrade($dbVersion)
	{
		if($dbVersion < 1.1)
		{
			echo 'Current Version is '.$dbVersion.'. New Version is 1.1. OK, Finish upgrade<br>';
		}
	}

	public function boot()
	{

	}

	public function shutdown()
	{

	}

} // END class Initialize
