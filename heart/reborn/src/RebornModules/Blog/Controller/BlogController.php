<?php

namespace Blog\Controller;

/**
 * This is Controller Class for Blog Module
 *
 * @author Nyan Lynn Htut
 * @copyright MIT License
 */

class BlogController extends \Reborn\Cores\Controller
{
	public function before()
	{
		//echo '-----===== I am Before =====-----<br>';
		$ev1 = array('name' => 'dummper', 'callback' => 'dumpp');
		$ev2 = array('name' => 'dummper', 'callback' => 'dumpp2');
		$ev3 = array('name' => 'dummper', 'callback' => 'MyEtest::dumpp');
		$ev = array($ev1, $ev2, $ev3);
		//dump($ev);
		//\Event::initialize($ev);

	}

	/**
	 *
	 */
	public function index()
	{
		return 'I am Blog Index Function<br>';
	}

	public function viewAction($file, $age, $job)
	{
		echo '<br>I am Blog View Method. I am '.$file.' Age is '.$age.'. Job is '.$job.'<br>';
		$d = Model_Blog::getById();
		return $d;
	}

	public function auth()
	{
		echo '<p style="color: #990000;">I am Authentication Method. </p><br>';
	}

	public function notify($p, $p2)
	{
		//echo '<p style="color: #009900;">I am Notify Method. </p><br>';
		//echo '<p style="color: #009900;">Param 1 = '.$p.'. </p><br>';
		//echo '<p style="color: #009900;">Param 2 = '.$p2.'. </p><br>';
		echo '<p style="color: #0000dd;">Call from after really calling.</p><br>';
	}

	/**
	 * @once
	 */
	public function callme($f, $s, $l)
	{
		//echo '<p style="color: #009900;">I am Callme Method. </p><br>';
		$fn = $f.' '.$s.' '.$l;
		//echo '<p style="color: #009900;">Hello '.$fn.'. </p><br>';
		//echo $fn;

	}

	/**
	 * This is Inspector Testing
	 * @before auth
	 * @after notify(Param1|Param2), callme(Nyan|Lynn|Htut), callme(Sir|Yan|Naing)
	 */
	public function categoryAction($name)
	{
		//dump(\Event::getAll());
		if(\Event::has('dummper'))
		{
			\Event::remove('dummper.dumpp');
			dump(\Event::getAll());
			\Event::call('dummper', array('name' => ucfirst($name), 'desc' => 'I am category description'));
		}


		return array('name' => ucfirst($name), 'desc' => 'I am category description');
	}

	public function after()
	{
		//echo '<br>-----===== I am After =====-----<br>';
	}
}
