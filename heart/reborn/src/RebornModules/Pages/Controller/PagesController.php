<?php

namespace Pages\Controller;

use Pages\Model\Page as Page;

class PagesController extends \Reborn\Cores\Controller
{
	//protected $useOrm = true;
	public function before()
	{
		parent::before();
		//return '-----===== I am Before for page module =====-----<br>';
	}

	public function index()
	{
		//$in = $this->input()->get('name');
		//dump($in);

		\Setting::set('site_title', 'Myanmar CMS');

		$o = '<form action="'.\Uri::create('pages/install').'" method="POST">';
		$o .= '<label>Module Name : </label>';
		$o .= '<input type="text" name="modname">';
		$o .= '<input type="submit" value="Install">';
		$o .= '</form>';

		$o .= '<hr>';

		$o .= '<form action="'.\Uri::create('pages/uninstall').'" method="POST">';
		$o .= '<label>Module Name : </label>';
		$o .= '<input type="text" name="modname">';
		$o .= '<input type="submit" value="UnInstall">';
		$o .= '</form>';


		/*$ps = Page::find(1);

		$o = '<h3>I am ORM</h3>';
		$o .= '<ul style="color: #345678;">';
		$o .= "<li>$ps->id</li>";
		$o .= "<li>$ps->slug</li>";
		$o .= "<li>$ps->name</li>";
		$o .= "<li>$ps->description</li>";
		$o .= "<li>$ps->enabled</li>";
		$o .= "<li>$ps->version</li>";
		$o .= '</ul>';*/

		/*$ms = \Pages\Model\PageModel::get(1);
		$o .= '<h3>I am Model</h3>';
		$o .= '<ul style="color: #760078;">';
		$o .= "<li>$ms->id</li>";
		$o .= "<li>$ms->slug</li>";
		$o .= "<li>$ms->name</li>";
		$o .= "<li>$ms->description</li>";
		$o .= "<li>$ms->enabled</li>";
		$o .= "<li>$ms->version</li>";
		$o .= '</ul>';*/

		$h = '<h1>'.\Setting::get('site_title').'</h1>';
		$h .= '<h1>Default Module is "'.\Setting::get('default_module').'"</h1>';
		//dump(base_convert(sha1(uniqid(mt_rand(), true)), 7, 36));
		//return \Redirect::to('/pages/view/bio/');
		return $h.'Page index use ORM and insert ID is <br>'.$o;
	}

	public function install()
	{
		if(\Input::isPost())
		{
			$module = \Input::get('modname');
			//dump($module, true);
			\Reborn\Cores\Module::install($module);
		}

		return 'Hello World';
	}

	public function uninstall()
	{
		if(\Input::isPost())
		{
			$module = \Input::get('modname');
			//dump($module, true);
			\Reborn\Cores\Module::uninstall($module);
		}

		return 'Bye Bye World';
	}

	public function view($file)
	{
		$path = realpath(__DIR__.'/../views/').DS.$file.EXT;

		if(file_exists($path))
		{
			$content = \Reborn\Cores\File::getContent($path);
		}
		else
		{
			$content = 'No file exits';
		}
		echo $content;
		//echo 'I am Blog View Method. I am '.$file.' Age is '.$age.'. Job is '.$job;
	}

	public function after($response)
	{

		//$response .= '<br>-----===== I am After for page module =====-----<br>';

		return parent::after($response);
	}
}
