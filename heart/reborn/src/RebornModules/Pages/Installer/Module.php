<?php

return array(

		// Information for this module (array)
		'info' => array(
				'name' => 'Page',
				'description' => 'Page module for Reborn CMS',
				'version' => '1.0',
				'author' => 'Nyan Lynn Htut',
				'author_email' => 'lynnhtut87@gmail.com',
				'author_url' => 'http://www.myanmarlinks.net'
			),

		// Role for this module
		'roles' => array(
				'create',
				'edit',
				'delete'
			),

		// Dependency for this module
		'require' => array(
				'reborn' => '2.0',
			),
	);
